#include "nodo.h"

class Arbol_Binario_Entero{
    private: // Atributos y Metodos Privados
        Nodo *raiz;
        Nodo *insertar(Estudiante Alumno , Nodo *root){ // Se Inserta Un Nodo en el Arbol Binario
            // Se verifica si root es NULL
            if(root == NULL){
                // Crea un espacio en memoria para crear un Nodo que tiene como dato un Objeto Estudiante
                return new Nodo(Alumno);
            }else if (Alumno.GetPromedio() < root -> GetProm()){ // Si el Valor es menor al de la raiz va hacia la izquierda
                root->Izquierda = insertar(Alumno,root->Izquierda);
            }else if (Alumno.GetPromedio() > root -> GetProm()){ // Si el Valor es mayor al de la raiz va hacia la derecha
                root->Derecha = insertar(Alumno, root->Derecha);
            }
            return root;
        }

        void Pre(Nodo *raiz){ // (raíz, izquierdo, derecho)
            // Se verifica si la raiz no es NULL
            if (raiz != NULL){
                // Este recorrido se realiza así: primero visita la raíz; segundo recorre el subárbol izquierdo y por último va a subárbol derecho.
                cout << " [" << raiz->GetProm() << "] " ;
                Pre(raiz->Izquierda);
                Pre(raiz->Derecha);
            }
        }

        void In(Nodo *raiz){ //  (izquierdo, raíz, derecho)
            // Se verifica si la raiz no es NULL
            if (raiz != NULL){
                // Este recorrido se realiza así: primero recorre el subárbol izquierdo, segundo visita la raíz y por último, va al subárbol derecho.
                In(raiz->Izquierda);
                cout << " [" << raiz->GetProm() << "] " ;
                In(raiz->Derecha);
            }
        }

        void Post(Nodo *raiz){ // (izquierdo, derecho, raíz)
        // Se verifica si la raiz no es NULL
            if (raiz != NULL){
                // Este recorrido se realiza así: primero recorre el subárbol izquierdo; segundo, recorre el subárbol derecho y por último, visita la raíz
                Post(raiz->Izquierda);
                Post(raiz->Derecha);
                cout << " [" << raiz->GetProm() << "] " ;
            }
        }

        bool Buscar(string codigo, float ValorBusqueda, Nodo *raiz){ // Buscar un Estudiante mediante su Matricula y Promedio
            if (raiz != NULL){
                if( (codigo.compare(raiz->GetMat()) == 0) && (ValorBusqueda == raiz->GetProm())){ // Condicion si alguna raiz tiene igual matricula y promedio que se busca
                    cout << "----------------------------------------------------------------------------" << endl;
                    cout << "Estudiante " << raiz->GetNombre() << " - Matricula: " << raiz->GetMat() << " - Promedio: " << raiz->GetProm();  // Impresion de Estudiante y todos sus aspectos
                    return true;
                }else if(ValorBusqueda > raiz->GetProm()){ // Si el promedio ingresado es mayor al de la raiz respectivamente
                    return Buscar(codigo,ValorBusqueda,raiz->Derecha); // Va hacia la derecha
                }else if(ValorBusqueda < raiz->GetProm()){ // Si el promedio ingresado es menor al de la raiz respectivamente
                    return Buscar(codigo,ValorBusqueda,raiz->Izquierda); // Va hacia la izquierda
                }else{ // Los parametros de busqueda no se encuentra alguna coincidencia
                    cout << "----------------------------------------------------------------------------" << endl;
                    cout << "Estudiante " ; 
                    return false;
                }
            }
            cout << "----------------------------------------------------------------------------" << endl;
            cout << "Estudiante " ; 
            return false; // Default
        }

    public:
        Arbol_Binario_Entero(){ // Constructor Base (Sin Parametros)
            raiz = NULL;
        }

        void PreOrden(){ // Metodo de PreOrden 
            Pre(raiz);
        }

        void InOrden(){ // Metodo de InOrden
            In(raiz);
        }

        void PostOrden(){ // Metodo de PostOrden
            Post(raiz);
        }

        void Agregar(Estudiante Alumno){ // Metodo que llama a otro metodo privado para ingresar un Estudiante en la raiz
            raiz = insertar(Alumno,raiz);
        }

        bool Busqueda(string codigo,float ValorBusqueda){ //  Metodo que llama a otro metodo privado para Buscar un Estudiante en la raiz mediante los parametros ingresados
            return Buscar(codigo,ValorBusqueda,raiz);
        }

};
