#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <ctype.h>
using namespace std;

// Clase Estudiante
class Estudiante{
    private: 
        // Atributos
        string Nombre;
        float Promedio;
        string Matricula;
        // La Clase Nodo es declarada como amigo de la Clase Estudiante
        // Puede acceder a los atributos privados y protegidos de la clase, pero aun se mantiene la privacidad de los atributos de la clase
        friend class Nodo;

    public:
        Estudiante(){ // Constructor Base (Sin Parametros)
            Nombre = " ";
            Promedio = 0;
            Matricula = "2021430001";
        }
        Estudiante(string nombre, float prom, string matricula){ // Constructor Base (Con Parametros)
            // Insertacion de parametros en los atributos
            this->Nombre = nombre;
            this->Promedio = prom;
            this->Matricula = matricula;
        }
        string GetNombre(){ // Metodo Que Retorna El Nombre del Estudiante
            return Nombre;
        }
        float GetPromedio(){ // Metodo Que Retorna El Promedio del Estudiante
            return Promedio;
        }
        string GetMatricula(){ // Metodo Que Retorna La Matricula del Estudiante
            return Matricula;
        }
};



