#include "Arbol_Binario.h"

int main(){
    int NumMenu;
    string Matricula_Num;
    Arbol_Binario_Entero raiz;
    string Nombre, Matricula;
    float Nota_final;
    
    // Menu Interactivo
    do{ 
        // Opciones correspondientes
        cout << "\n\n\t-- Menu Arbol Binario --\n"<< endl;
        cout << "[1]. Insertar Estudiante" << endl;
        cout << "[2]. Busqueda" << endl;
        cout << "[3]. Pre-orden " << endl;
        cout << "[4]. In-orden" << endl;
        cout << "[5]. Post-orden." << endl;
        cout << "[6]. Salir." << endl;
        cout << "#> Ingrese su opcion: ";
        cin >> NumMenu;

        switch (NumMenu)
        {
            case 1:// Caso 1 de ingresar un Estudiante
            {
                cout << "--> Ingrese su Nombre: ";
                cin >> Nombre;
                cout << "--> Ingrese su Promedio (tipo float o entero): ";
                cin >> Nota_final;
                cout << "--> Ingrese su Matricula (ej: 20): ";
                cin >> Matricula_Num;
                while (Matricula.find(Matricula_Num) != string::npos){ // No se repita una matricula
                    cout << "\n\n-------------- Advertencia --------------" << endl;
                    cout << "Ya existe un estudiante que empieza o contiene esa matricula" << endl;
                    cout << "Ingrese su Matricula (ej: 20): ";
                    cin >> Matricula_Num;
                    cout << "-----------------------------------------" << endl;
                }
                Matricula += Matricula_Num;
                // Creacion de Objeto Estudiante
                Estudiante p(Nombre,Nota_final,Matricula_Num);
                // Agregar Estudiante al Arbol Binario
                raiz.Agregar(p);
                break;
            }
            case 2:// Caso de Busqueda de Estudiante mediante su promedio y matricula
                cout << "Ingrese el Promedio a Buscar (tipo float o entero): ";
                cin >> Nota_final;
                cout << "Ingrese la Matricula a Buscar (ej: 20): ";
                cin >> Matricula_Num;
                cout << "\n\n";
                // Condicion si el Estudiante fue encontrado o no
                if (raiz.Busqueda(Matricula_Num,Nota_final)) cout << " Fue Encontrado" << endl; else cout << " No Fue Encontrado" << endl;
                cout << "----------------------------------------------------------------------------" << endl;
                break;
            case 3: // Recorrido Preorden en el Arbol Binario
                cout << "\n\n";
                cout << "* Preorden: ";
                raiz.PreOrden();
                break;
            case 4: // Recorrido Inorden en el Arbol Binario
                cout << "\n\n";
                cout << "* Inorden: ";
                raiz.InOrden();
                break;
            case 5: // Recorrido Postorden en el Arbol Binario
                cout << "\n\n";
                cout << "* Postorden: ";
                raiz.PostOrden();
                break;
            case 6: // Finalización de la Ejecucion
                return 0;
            default: // Ingreso de un numero no correspondiente a los casos anteriores
                break;
        }
    }while(NumMenu != 6);
    return 0;
}
