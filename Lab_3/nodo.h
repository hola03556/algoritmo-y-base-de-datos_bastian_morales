#include "Estudiante.h"

// Clase Nodo
class Nodo{
    private: // Atributos
        Estudiante Alumno;
        Nodo *Izquierda;
        Nodo *Derecha;
        // La Clase Arbol_Binario_Entero es declarada como amigo de la Clase Nodo
        // Puede acceder a los atributos privados y protegidos de la clase, pero aun se mantiene la privacidad de los atributos de la clase 
        friend class Arbol_Binario_Entero; // Legal

    public:
        Nodo(Estudiante alumno){ // Constructor Base (Con Parametros correspondientes)
            this->Alumno = alumno;
            this->Izquierda = NULL;
            this->Derecha = NULL;
        }

        float GetProm(){ // Metodo que entrego el Promedio del Estudiente desde la clase Estudiante
            return Alumno.GetPromedio();
        }
        string GetMat(){ // Metodo que entrego el Matricula del Estudiente desde la clase Estudiante
            return Alumno.GetMatricula();
        }
        string GetNombre(){ // Metodo que entrego el Nombre del Estudiente desde la clase Estudiante
            return Alumno.GetNombre();
        }
};
