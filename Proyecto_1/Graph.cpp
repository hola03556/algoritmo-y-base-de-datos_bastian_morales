#include "graph.h"

// Metodo Construccion Grafo 
template <class T>
GrafoTipo<T>::GrafoTipo(int maxV){
    numVertices = 0;
    maxVertices = maxV;
    vertices = new T[maxV];
    aristas = new int*[maxV];
    for(int i = 0; i < maxV; i++){
        aristas[i] = new int[maxV];
    }
    distancia = new int[maxV];
    marcas = new bool[maxV];
    adyacente = new list<int>[maxV];
    adyacente_costo = new list<int>[maxV];

}

// Metodo Desconstructor Grafo
template <class T>
GrafoTipo<T>::~GrafoTipo(){
    delete [] vertices;
    for(int i = 0; i < maxVertices; i++)
        delete [] aristas[i];
    delete [] aristas;
    delete [] distancia;
    delete [] marcas;
    delete [] adyacente;
    delete [] adyacente_costo;
} 

// Metodo Agregar Vertices
template <class T>
void GrafoTipo<T>::Agregar_Vertice(T vertice){
    vertices[numVertices] = vertice;
    for(int indice = 0; indice < numVertices; indice++) {
        aristas[numVertices][indice] = NULL_Arista;
        aristas[indice][numVertices] = NULL_Arista;
    }
    numVertices++;
}

// Metodo Agregar Aristas
template <class T>
void GrafoTipo<T>::Agregar_Arista(T DesdeVertice, T HastaVertice, int Costo){
    int fila, col;
    fila = IndiceEs(vertices, DesdeVertice);
    col = IndiceEs(vertices, HastaVertice);
    aristas[fila][col] = Costo;
}

// Metodo Entrega el costo entre dos Vertices
template <class T>
int GrafoTipo<T>::Costo(T DesdeVertice, T HastaVertice){
    int fila, col;
    fila = IndiceEs(vertices, DesdeVertice);
    col = IndiceEs(vertices, HastaVertice);
    return aristas[fila][col];
}

// Metodo Entrega el Indice en la interseccion de Ambos Vertices
template <class T>
int GrafoTipo<T>::IndiceEs(T *vertices, T vertice){
    for(int i = 0; i < numVertices; i++){
        if (vertices[i] == vertice){
            return i;
        }
    }
    return -1;
}

// Metodo Comprueba si el Grafo esta Lleno
template <class T>
bool GrafoTipo<T>::Esta_Llena(){
    if (numVertices == maxVertices)
        return true;
    return false;
}

// Metodo Comprueba si el Grafo esta vacio
template <class T>
bool GrafoTipo<T>::Esta_Vacia(){
    if (numVertices == 0)
        return true;
    return false;
}

// Metodo Que Entrega las cantidad de Filas
template <class T>
int GrafoTipo<T>::Tamanio_Grafo(){
    return numVertices;
}

template <class T>
int GrafoTipo<T>::Distancia_Minima(){
    int minimo = INFINITO, indice_minimo;
    for (int i = 0; i < maxVertices; i++){
        if (!marcas[i] && distancia[i] <= minimo){
            minimo = distancia[i], indice_minimo = i;
        }
    }
    return indice_minimo;
}

template <class T>
void GrafoTipo<T>::Mostrar_Seguimiento(){

    string Vertice_Nom, Costo_Min;
    cout << "Vertice\tRecorrido Corto" << endl;
    for (int i=0; i < maxVertices; i++){
        if (i < numVertices){
            Vertice_Nom = vertices[i];
        }else{
            Vertice_Nom = "--";
        }
        if (distancia[i] == INFINITO)
            Costo_Min = "Infinito";
        else
            Costo_Min = to_string(distancia[i]);
        cout << "[" << i+1 << "] " << Vertice_Nom << "\t" << Costo_Min << endl;
    }
}

template <class T>
void GrafoTipo<T>::Dijkstra(T origen){
    // Si el vertice origen o que se quiere empezar no es el primer vertice ingresado
    int indice_raiz = IndiceEs(vertices, origen);
    // Se inicializa todos las distancias con "INFINITO" y el arreglo marcas con false
    for (int i = 0; i < maxVertices; i++){
        distancia[i] = INFINITO, marcas[i] = false;
    }
    distancia[indice_raiz] = 0;
    for (int k = 0; k < maxVertices - 1; k++){
        int auxiliar = Distancia_Minima();
        marcas[auxiliar] = true;
        for ( int i = 0; i < maxVertices; i++){
            if (!marcas[i] && aristas[auxiliar][i] && distancia[auxiliar] != INFINITO && distancia[auxiliar] + aristas[auxiliar][i] < distancia[i]){
                    distancia[i] = distancia[auxiliar] + aristas[auxiliar][i];
            }
        }
    }
}







template <class T>
void GrafoTipo<T>::Agregar_Arista_B(int u, int v, int peso){
    adyacente[u].push_back(v); // Agrega V en una lista u
    adyacente_costo[u].push_back(peso); // Agrega the peso o costo de el camino bueno
}
template <class T>
void GrafoTipo<T>::Imprimir_Todos_Caminos(int s, int d)
{
    // Marca todos los vertices no visitados
    bool* visitados = new bool[maxVertices];
    // Crea una lista para los tipos de caminos
    int* Camino = new int[maxVertices];
    int* Camino_costo = new int[maxVertices];
    int Camino_indice = 0; // Inicializa  camino[] y camino_costo[] en vacio

    // Inicializa todas los vertices no visitados
    for (int i = 0; i < maxVertices; i++)
        visitados[i] = false;

    // Llama la funcion recursiva a imprimir todos los caminos
    // Inicializa el costo en 0 de la ciudad inicial
    Imprimir_Todos_Caminos_Util(s, d, visitados, Camino_costo, Camino, Camino_indice, 0);
}

// Un funcion recursiva que imprime todos los caminos desde "u" a "d"
// visitados[] guarda los vertices actuales
template <class T>
void GrafoTipo<T>::Imprimir_Todos_Caminos_Util(int u, int d, bool visitados[], int Camino_costo[],int Camino[], int& Camino_indice, int costo)
{
    // Marca el  nodo actual y  guarda in camino[]
    visitados[u] = true;
    Camino[Camino_indice] = u;
    Camino_costo[Camino_indice] = costo;   // Guarda el costo  de este paso
    Camino_indice++;
    int sum = 0;
    int camino = false;
    // si vertice actual es mismo al  destino, se imprime
    // actaul camino[]
    if (u == d) {
        cout << "# Inicio # -> ";
        for (int i = 0; i < Camino_indice; i++){
            sum += Camino_costo[i];   // Ahora agrega todos los costos
            //cout << v1[Camino[i]] << " ";
            if (i != Camino_indice-1){
                cout << v1[Camino[i]] << " = ";
            }else{
                cout << v1[Camino[i]];
            }
        }
        cout << " -> " << "# Meta #" << endl;
        cout << "Distancia Total es: " << sum;
        cout << endl;
    }else // Si vertice actual no es el destino
    {
        // Repetir para todos los vértices adyacentes al vértice actual
        // Ahora pasamos por encima de adyacente y adyacente_costo
        list<int>::iterator i, j;
        for (i = adyacente[u].begin(), j = adyacente_costo[u].begin();
            i != adyacente[u].end(); ++i, ++j)
            if (!visitados[*i])
                Imprimir_Todos_Caminos_Util(*i, d, visitados, Camino_costo, Camino, 
                                Camino_indice, *j);
    }

    // Quitar el vértice actual de camino[] y marcarlo como no visitado
    Camino_indice--;
    visitados[u] = false;
}
