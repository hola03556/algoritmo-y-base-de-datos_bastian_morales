const int NULL_Arista = 0;
const int INFINITO = 999999;
#define tamanio 76
#include <iostream>
#include <string>
#include <stdlib.h>
#include <list>
#include <vector>

using namespace std;
const vector <string> v1 = {"PD-1", "CTLA4", "CD45", "CD48", "CD3e-CD3y", "Antigeno", "TCRa", "TCRb", "CD3E-CD3E","CD38","CD40L",
                                    "ICOS", "CD28", "SHP1", "LCK", "CBL", "Ubiquitin Mediated Proteolysis", "FYN", "ZAP70","Dlgl", "p38",
                                    "SLP-76", "GADS", "LAT", "NCK", "PAK", "VAV", "Regulation of Actian Cytoske Leton","Regulation of actin cytoskeleton","Ubiquitin mediated proteolysis","Proliferation Differentiation Immuno response",
                                    "VAV", "GRB2", "PLC-y1", "SOS", "ITK", "RhoCel42", "CaN", "NRAT", "RasGRP1", "Ras",
                                    "Raf", "MEK12", "Erk", "PKC8", "CARMA1-BCL-10-MALT1", "TAK1", "MKK7", "JNK2","NFAT", "IKKb-IKKy-IKKa", 
                                    "PI3K", "GRB2", "PDK1", "AKT", "COT", "NIK", "GSK-3b", "NFkB","IkB", "AP1", 
                                    "Anergy" ,"IL-2", "IL-4", "IL-5", "IL-10", "IFN-y", "GM-CSF", "TNFa","CDK4","RhoCdc42",
                                    "DAG","Ca2+","IP3","PIP3","DNA"};
const vector <int> w = {};



template <class T>
// Clase GrafoTipo
class GrafoTipo{
    // Metodos de la clase
    public:
        GrafoTipo(int);
        ~GrafoTipo();
        void Agregar_Vertice(T);
        void Agregar_Arista(T, T, int);
        int Costo(T, T);
        int IndiceEs(T*, T);
        bool Esta_Vacia();
        bool Esta_Llena(); 
        int Tamanio_Grafo();
        int Distancia_Minima();
        void Dijkstra(T);
        void Mostrar_Seguimiento();

        void Agregar_Arista_B(int,int,int);
        void Imprimir_Todos_Caminos(int,int);
        void Imprimir_Todos_Caminos_Util(int, int, bool*, int*, int*, int&, int);
    // Atributos de la clase
    private:
        int numVertices;
        int maxVertices;
        T* vertices;
        int **aristas;
        bool* marcas;
        int *distancia;
        list<int>* adyacente;
        list<int>* adyacente_costo;

}; 
