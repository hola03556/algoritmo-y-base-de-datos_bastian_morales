/*
    Proyecto T cell Receptor Signaling Pathway
    Integrantes: Bastián Morales - Thomas Aranguiz
*/


#include "Graph.cpp"
int main() {
    int NumMenu, inicio, meta;
    int Tamanio = 76; // 72

    string caracteres[74] = {"PD-1", "CTLA4", "CD45", "CD48", "CD3e-CD3y", "Antigeno", "TCRa", "TCRb", "CD3E-CD3E","CD38",
                        "CD40L", "ICOS", "CD28", "SHP1", "LCK", "CBL", "Ubiquitin Mediated Proteolysis", "FYN", "ZAP70","Dlgl",
                        "p38", "SLP-76", "GADS", "LAT", "NCK", "PAK", "VAV", "Regulation of Actian Cytoske Leton","Regulation of actin cytoskeleton","Ubiquitin mediated proteolysis",
                        "Proliferation Differentiation Immuno response","GRB2", "PLC-y1", "SOS", "ITK", "RhoCel42", "CaN", "NRAT", "RasGRP1", "Ras",
                        "Raf","MEK12", "Erk", "PKC8", "CARMA1-BCL-10-MALT1", "TAK1", "MKK7", "JNK2","NFAT", "IKKb-IKKy-IKKa", 
                        "PI3K","PDK1", "AKT", "COT", "NIK", "GSK-3b", "NFkB","IkB", "AP1", "Anergy",
                        "IL-2", "IL-4", "IL-5", "IL-10", "IFN-y", "GM-CSF","TNFa","CDK4","RhoCdc42","DAG",
                        "Ca2+","IP3","PIP3","DNA"};

    // Insertacion de vertices


        cout << "\n\n\t-- Menu T cell Receptor Signaling Pathway --\n"<< endl;
        cout << "[1]. imprimir el recorrido más corto desde un nodo a cada nodo " << endl;
        cout << "[2]. Imprimir todos los caminos de un nodo origen al nodo destino " << endl;
        cout << "[0]. Salir " << endl;
        cout << "Eleccion: ";
        cin >> NumMenu;
        
        switch(NumMenu)
        {
            case 1:
            {   
                GrafoTipo<string> grafo(74);
                for (int i = 0; i < 74; i++){
                    grafo.Agregar_Vertice(caracteres[i]);
                }
                grafo.Agregar_Arista("PD-1","SHP1",2); //0
                grafo.Agregar_Arista("SHP1","PD-1",2); //0
                grafo.Agregar_Arista("CTLA4","SHP1",2); //0
                grafo.Agregar_Arista("SHP1","CTLA4",2); //0
                grafo.Agregar_Arista("SHP1","LCK",2); //0
                grafo.Agregar_Arista("LCK","SPHI",2); //0
                grafo.Agregar_Arista("CD45","LCK",1); //-1
                grafo.Agregar_Arista("CD45","FYN",1); //-1
                grafo.Agregar_Arista("CD48","LCK",2); //0
                grafo.Agregar_Arista("LCK","CD48",2); //0
                grafo.Agregar_Arista("LCK","FYN",2); //0
                grafo.Agregar_Arista("FYN","LCK",2); //0
                grafo.Agregar_Arista("LCK","ZAP70",3); //1
                grafo.Agregar_Arista("CD3e-CD3y","FYN",3); //1
                grafo.Agregar_Arista("FYN","CD3e-CD3y",3); //1
                grafo.Agregar_Arista("CD3E-CD3E","FYN",3); //1
                grafo.Agregar_Arista("FYN","CD3E-CD3E",3); //1
                grafo.Agregar_Arista("CD3E-CD3E","ZAP70",2); //0
                grafo.Agregar_Arista("ZAP70","CD3E-CD3E",3); //1
                grafo.Agregar_Arista("ZAP70","p38",3); //1
                grafo.Agregar_Arista("Dlgl","p38",2); //0
                grafo.Agregar_Arista("p38","Dlgl",2); //0
                grafo.Agregar_Arista("ZAP70","SLP-76",3); //1
                grafo.Agregar_Arista("ZAP70","LAT",3); //1
                grafo.Agregar_Arista("LAT","GADS",2); //0
                grafo.Agregar_Arista("GADS","LAT",2); //0
                grafo.Agregar_Arista("GADS","SLP-76",2); //0
                grafo.Agregar_Arista("SLP-76","GADS",2); //0
                grafo.Agregar_Arista("LAT","GRB2",2); //0
                grafo.Agregar_Arista("GRB2","LAT",2); //0
                grafo.Agregar_Arista("GRB2","SOS",2); //0
                grafo.Agregar_Arista("LAT","PLC-y1",2); //0
                grafo.Agregar_Arista("PLC-y1","LAT",2); //0
                grafo.Agregar_Arista("SOS","RasGRP1",2); //0
                grafo.Agregar_Arista("PLC-y1","DAG",2);//////////   //0
                grafo.Agregar_Arista("PLC-y1","IP3",2);//////////   //0
                grafo.Agregar_Arista("IP3","Ca2+",2);///////////    //0
                grafo.Agregar_Arista("SLP-76","PLC-y1",2); //0
                grafo.Agregar_Arista("PLC-y1","SLP-76",2); //0
                grafo.Agregar_Arista("SLP-76","ITK",2); //0
                grafo.Agregar_Arista("ITK","SLP-76",2); //0
                grafo.Agregar_Arista("SLP-76","NCK",2); //0
                grafo.Agregar_Arista("NCK","SLP-76",2); //0
                grafo.Agregar_Arista("SLP-76","VAV",2); //0
                grafo.Agregar_Arista("VAV","SLP-76",2); //0
                grafo.Agregar_Arista("NCK","PAK",3); //1
                grafo.Agregar_Arista("VAV","RhoCdc42",2); //0
                grafo.Agregar_Arista("ITK","VAV",3); //0
                grafo.Agregar_Arista("ITK","PLC-y1",3); //0
                grafo.Agregar_Arista("PAK","Regulation of actin cytoskeleton",2); //0
                grafo.Agregar_Arista("RhoCdc42","Regulation of actin cytoskeleton",2); //0
                grafo.Agregar_Arista("DAG","RasGRP1",2);////  //0
                grafo.Agregar_Arista("DAG","PKC8",2);////    //0
                grafo.Agregar_Arista("RasGRP1","Ras",2);  //0
                grafo.Agregar_Arista("Ras","Raf",2); //0
                grafo.Agregar_Arista("Raf","MEK12",3); //1
                grafo.Agregar_Arista("MEK12","Erk",3); //1
                grafo.Agregar_Arista("Ca2+","CaN",2);///   //0
                grafo.Agregar_Arista("CaN","NFAT",1); //-1
                grafo.Agregar_Arista("p38","NFAT",2); //0
                grafo.Agregar_Arista("Erk","AP1",3); //1
                grafo.Agregar_Arista("PKC8","TAK1",2); //0
                grafo.Agregar_Arista("PKC8","CARMA1-BCL-10-MALT1",3);  //1
                grafo.Agregar_Arista("CARMA1-BCL-10-MALT1","TAK1",2); //0
                grafo.Agregar_Arista("TAK1","CARMA1-BCL-10-MALT1",2); //0
                grafo.Agregar_Arista("TAK1","MKK7",3); //1
                grafo.Agregar_Arista("TAK1","IKKb-IKKy-IKKa",3); //1
                grafo.Agregar_Arista("MKK7","JNK2",3); //1
                grafo.Agregar_Arista("JNK2","AP1",3); //1
                grafo.Agregar_Arista("ICOS","CD40L",2); //0
                grafo.Agregar_Arista("ICOS","PI3K",2); //0
                grafo.Agregar_Arista("CD28","PI3K",2); //0
                grafo.Agregar_Arista("CD28","GRB2",2); //0
                grafo.Agregar_Arista("PI3K","PIP3",2);////   //0
                grafo.Agregar_Arista("PIP3","PDK1",2);////  //0
                grafo.Agregar_Arista("PIP3","AKT",2);////   //0
                grafo.Agregar_Arista("PDK1","AKT",3); //1
                grafo.Agregar_Arista("AKT","COT",3); //1
                grafo.Agregar_Arista("COT","NIK",3); //1
                grafo.Agregar_Arista("NIK","IKKb-IKKy-IKKa",3); //1
                grafo.Agregar_Arista("IKKb-IKKy-IKKa","IkB",3); //1
                grafo.Agregar_Arista("IkB","Ubiquitin mediated proteolysis",2); //0
                grafo.Agregar_Arista("NFkB","IkB",2);   //0
                grafo.Agregar_Arista("NFkB","ADN",2);   //0
                grafo.Agregar_Arista("AP1","DNA",2);/////////   //0
                grafo.Agregar_Arista("NFAT","DNA",2);//////   //0
                grafo.Agregar_Arista("DNA","Anergy",2);//////   //0
                grafo.Agregar_Arista("DNA","Proliferation Differentiation Immuno response",2);  //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","IL-2",2);  //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","IL-4",2);  //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","IL-5",2);  //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","IL-10",2);  //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","IFN-y",2);  //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","GM-CSF",2);  //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","TNFa",2);   //0
                grafo.Agregar_Arista("Proliferation Differentiation Immuno response","CDK4",2);  //0
                do
                {
                    cout << "\t\n\n************* Opciones ******************" << endl;
                    for (int i = 0; i < 74; i++){
                        cout << "[" << i+1 << "] " << caracteres[i] << endl;
                    }
                    cout << "# Escoja un numero: ";
                    cin >> inicio;
                    if (inicio <= 0 || inicio >= 74 + 1){
                        cout << "**************************************" << endl;
                        cout << " Numero no Valido. Intentelo de nuevo " << endl;
                        cout << "**************************************" << endl;
                    }
                } while (inicio <= 0 || inicio >= 74 + 1 );
                cout << "\n**************************************" << endl;
                string posicion_inicio = caracteres [inicio - 1];
                grafo.Dijkstra(posicion_inicio);
                grafo.Mostrar_Seguimiento();
                cout << "\n**************************************" << endl;
                grafo.~GrafoTipo();
                //break;
            }
            case 2:
            {   
                GrafoTipo<int> grafo(76);
                grafo.Agregar_Arista_B(0,13,2); //0
                grafo.Agregar_Arista_B(13,0,2); //0
                grafo.Agregar_Arista_B(1,13,2); //0
                grafo.Agregar_Arista_B(13,1,2); //0
                grafo.Agregar_Arista_B(13,14,2); //0
                grafo.Agregar_Arista_B(14,13,2); //0
                grafo.Agregar_Arista_B(2,14,1); //-1
                grafo.Agregar_Arista_B(2,17,1); //-1
                grafo.Agregar_Arista_B(3,17,2); //0
                grafo.Agregar_Arista_B(17,3,2); //0
                grafo.Agregar_Arista_B(14,17,2); //0
                grafo.Agregar_Arista_B(17,14,2); //0
                grafo.Agregar_Arista_B(14,18,3); //1
                grafo.Agregar_Arista_B(4,17,3); //1
                grafo.Agregar_Arista_B(17,4,3); //1
                grafo.Agregar_Arista_B(8,17,3); //1
                grafo.Agregar_Arista_B(17,8,3); //1
                grafo.Agregar_Arista_B(8,18,2); //0
                grafo.Agregar_Arista_B(18,8,3); //1
                grafo.Agregar_Arista_B(18,20,3); //1
                grafo.Agregar_Arista_B(19,20,2); //0
                grafo.Agregar_Arista_B(20,19,2); //0
                grafo.Agregar_Arista_B(18,21,3); //1
                grafo.Agregar_Arista_B(18,23,3); //1
                grafo.Agregar_Arista_B(23,22,2); //0
                grafo.Agregar_Arista_B(22,23,2); //0
                grafo.Agregar_Arista_B(22,21,2); //0
                grafo.Agregar_Arista_B(21,22,2); //0
                grafo.Agregar_Arista_B(23,31,2); //0
                grafo.Agregar_Arista_B(31,23,2); //0
                grafo.Agregar_Arista_B(31,33,2); //0
                grafo.Agregar_Arista_B(23,32,2); //0
                grafo.Agregar_Arista_B(32,23,2); //0
                grafo.Agregar_Arista_B(33,38,2); //0
                grafo.Agregar_Arista_B(32,69,2);//////////   //0
                grafo.Agregar_Arista_B(32,71,2);//////////   //0
                grafo.Agregar_Arista_B(71,70,2);///////////    //0
                grafo.Agregar_Arista_B(21,32,2); //0
                grafo.Agregar_Arista_B(32,21,2); //0
                grafo.Agregar_Arista_B(21,34,2); //0
                grafo.Agregar_Arista_B(34,21,2); //0
                grafo.Agregar_Arista_B(21,24,2); //0
                grafo.Agregar_Arista_B(24,21,2); //0
                grafo.Agregar_Arista_B(21,26,2); //0
                grafo.Agregar_Arista_B(26,21,2); //0
                grafo.Agregar_Arista_B(24,25,3); //1
                grafo.Agregar_Arista_B(26,68,2); //0
                grafo.Agregar_Arista_B(34,26,3); //0
                grafo.Agregar_Arista_B(34,21,3); //0
                grafo.Agregar_Arista_B(25,28,2); //0
                grafo.Agregar_Arista_B(68,28,2); //0
                grafo.Agregar_Arista_B(69,38,2);////  //0
                grafo.Agregar_Arista_B(69,43,2);////    //0
                grafo.Agregar_Arista_B(38,39,2);  //0
                grafo.Agregar_Arista_B(39,40,2); //0
                grafo.Agregar_Arista_B(40,41,3); //1
                grafo.Agregar_Arista_B(41,42,3); //1
                grafo.Agregar_Arista_B(70,36,2);///   //0
                grafo.Agregar_Arista_B(36,48,1); //-1
                grafo.Agregar_Arista_B(20,48,2); //0
                grafo.Agregar_Arista_B(42,58,3); //1
                grafo.Agregar_Arista_B(43,45,2); //0
                grafo.Agregar_Arista_B(43,44,3);  //1
                grafo.Agregar_Arista_B(44,45,2); //0
                grafo.Agregar_Arista_B(45,44,2); //0
                grafo.Agregar_Arista_B(45,46,3); //1
                grafo.Agregar_Arista_B(45,49,3); //1
                grafo.Agregar_Arista_B(46,47,3); //1
                grafo.Agregar_Arista_B(47,58,3); //1
                grafo.Agregar_Arista_B(11,10,2); //0
                grafo.Agregar_Arista_B(11,50,2); //0
                grafo.Agregar_Arista_B(12,50,2); //0
                grafo.Agregar_Arista_B(12,31,2); //0
                grafo.Agregar_Arista_B(50,72,2);////   //0
                grafo.Agregar_Arista_B(72,51,2);////  //0
                grafo.Agregar_Arista_B(72,52,2);////   //0
                grafo.Agregar_Arista_B(51,52,3); //1
                grafo.Agregar_Arista_B(52,53,3); //1
                grafo.Agregar_Arista_B(53,54,3); //1
                grafo.Agregar_Arista_B(54,49,3); //1
                grafo.Agregar_Arista_B(49,57,3); //1
                grafo.Agregar_Arista_B(57,29,2); //0
                grafo.Agregar_Arista_B(56,57,2);   //0
                grafo.Agregar_Arista_B(56,73,2);   //0
                grafo.Agregar_Arista_B(58,73,2);/////////   //0
                grafo.Agregar_Arista_B(48,73,2);//////   //0
                grafo.Agregar_Arista_B(73,59,2);//////   //0
                grafo.Agregar_Arista_B(73,30,2);  //0
                grafo.Agregar_Arista_B(30,60,2);  //0
                grafo.Agregar_Arista_B(30,61,2);  //0
                grafo.Agregar_Arista_B(30,62,2);  //0
                grafo.Agregar_Arista_B(30,63,2);  //0
                grafo.Agregar_Arista_B(30,64,2);  //0
                grafo.Agregar_Arista_B(30,65,2);  //0
                grafo.Agregar_Arista_B(30,66,2);   //0
                grafo.Agregar_Arista_B(30,67,2);  //0
                do
                {
                    cout << "\t\n\n************* Opciones ******************" << endl;
                    for (int i = 0; i < 76; i++){
                        cout << " [" << i+1 << "]" << v1[i] << endl;
                    }
                    cout << "\n# Escoja un nodo origen: ";
                    cin >> inicio;
                    cout << "# Escoja un nodo destino: ";
                    cin >> meta;
                    if (inicio <= 0 || inicio >= 76 + 1 || meta <= 0 || meta >= 76+1){
                        cout << "\n**************************************" << endl;
                        cout << " Numero no Valido. Intentelo de nuevo " << endl;
                        cout << "**************************************" << endl;
                    }
                } while (inicio <= 0 || inicio >= 76 + 1 || meta <= 0 || meta >= 76+1);
                cout << "\n**************************************************************" << endl;
                cout << "Todos los caminos diferentes desde " << v1[inicio-1] << " a " << v1[meta-1] << endl;
                cout << "**************************************************************" << endl;
                // Metodo para imprimir los caminos
                grafo.Imprimir_Todos_Caminos(inicio-1, meta-1);
                cout << "\n**************************************************************" << endl;
                grafo.~GrafoTipo();
                //break;
            }
            case 0:
                return 0;
            default:
                break;
        }

    return 0;
}

