#include "Graph.cpp"
int main() {
    int Tamanio = 9;
    string caracteres[Tamanio] = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
    GrafoTipo<string> grafo(Tamanio);
    cout << "\n\n\t-- Grafo Mediante Algoritmo Dijkstra --\n"<< endl;
    // Insertacion de vertices
    for (int i = 0; i < Tamanio; i++)
        grafo.Agregar_Vertice(caracteres[i]);
    // Agregacion de aristas entre vertices
    grafo.Agregar_Arista("A", "B", 10);
    grafo.Agregar_Arista("A", "E", 1);
    grafo.Agregar_Arista("A", "D", 2);
    grafo.Agregar_Arista("B", "C", 3);
    grafo.Agregar_Arista("C", "B", 4);
    grafo.Agregar_Arista("D", "E", 0);
    grafo.Agregar_Arista("D", "G", 6);
    grafo.Agregar_Arista("D", "D", 4);
    grafo.Agregar_Arista("E", "F", 7);
    grafo.Agregar_Arista("E", "H", 1);
    grafo.Agregar_Arista("E", "B", 8);
    grafo.Agregar_Arista("F", "C", 8);
    grafo.Agregar_Arista("F", "B", 2);
    grafo.Agregar_Arista("G", "H", 2);
    grafo.Agregar_Arista("H", "F", 2);
    grafo.Agregar_Arista("H", "G", 5);
    grafo.Agregar_Arista("H", "D", 9);
    grafo.Agregar_Arista("I", "H", 6);
    grafo.Agregar_Arista("I", "F", 7);

    string base = "A";
    grafo.Dijkstra(base);
    grafo.Mostrar_Seguimiento();
    grafo.~GrafoTipo();

    return 0;
}