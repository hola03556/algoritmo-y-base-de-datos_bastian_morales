const int NULL_Arista = 0;
const int INFINITO = 999999;
#define tamanio 12
#include <iostream>
#include <string>
using namespace std;

template <class T>
// Clase GrafoTipo
class GrafoTipo{
    // Metodos de la clase
    public:
        GrafoTipo(int);
        ~GrafoTipo();
        void Agregar_Vertice(T);
        void Agregar_Arista(T, T, int);
        int Costo(T, T);
        int IndiceEs(T*, T);
        bool Esta_Vacia();
        bool Esta_Llena(); 
        int Tamanio_Grafo();
        int Distancia_Minima();
        void Dijkstra(T);
        void Mostrar_Seguimiento();
    // Atributos de la clase
    private:
        int numVertices;
        int maxVertices;
        T* vertices;
        int **aristas;
        bool* marcas;
        int *distancia;
}; 
