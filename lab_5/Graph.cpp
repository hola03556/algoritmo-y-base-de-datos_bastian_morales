#include "graph.h"

// Metodo Construccion Grafo 
template <class T>
GrafoTipo<T>::GrafoTipo(int maxV){
    numVertices = 0;
    maxVertices = maxV;
    vertices = new T[maxV];
    aristas = new int*[maxV];
    for(int i = 0; i < maxV; i++){
        aristas[i] = new int[maxV];
    }
    distancia = new int[maxV];
    marcas = new bool[maxV];
}

// Metodo Desconstructor Grafo
template <class T>
GrafoTipo<T>::~GrafoTipo(){
    delete [] vertices;
    for(int i = 0; i < maxVertices; i++)
        delete [] aristas[i];
    delete [] aristas;
    delete [] distancia;
    delete [] marcas;
} 

// Metodo Agregar Vertices
template <class T>
void GrafoTipo<T>::Agregar_Vertice(T vertice){
    vertices[numVertices] = vertice;
    for(int indice = 0; indice < numVertices; indice++) {
        aristas[numVertices][indice] = NULL_Arista;
        aristas[indice][numVertices] = NULL_Arista;
    }
    numVertices++;
}

// Metodo Agregar Aristas
template <class T>
void GrafoTipo<T>::Agregar_Arista(T DesdeVertice, T HastaVertice, int Costo){
    int fila, col;
    fila = IndiceEs(vertices, DesdeVertice);
    col = IndiceEs(vertices, HastaVertice);
    aristas[fila][col] = Costo;
}

// Metodo Entrega el costo entre dos Vertices
template <class T>
int GrafoTipo<T>::Costo(T DesdeVertice, T HastaVertice){
    int fila, col;
    fila = IndiceEs(vertices, DesdeVertice);
    col = IndiceEs(vertices, HastaVertice);
    return aristas[fila][col];
}

// Metodo Entrega el Indice en la interseccion de Ambos Vertices
template <class T>
int GrafoTipo<T>::IndiceEs(T *vertices, T vertice){
    for(int i = 0; i < numVertices; i++){
        if (vertices[i] == vertice){
            return i;
        }
    }
    return -1;
}

// Metodo Comprueba si el Grafo esta Lleno
template <class T>
bool GrafoTipo<T>::Esta_Llena(){
    if (numVertices == maxVertices)
        return true;
    return false;
}

// Metodo Comprueba si el Grafo esta vacio
template <class T>
bool GrafoTipo<T>::Esta_Vacia(){
    if (numVertices == 0)
        return true;
    return false;
}

// Metodo Que Entrega las cantidad de Filas
template <class T>
int GrafoTipo<T>::Tamanio_Grafo(){
    return numVertices;
}

template <class T>
int GrafoTipo<T>::Distancia_Minima(){
    int minimo = INFINITO, indice_minimo;
    for (int i = 0; i < maxVertices; i++){
        if (!marcas[i] && distancia[i] <= minimo){
            minimo = distancia[i], indice_minimo = i;
        }
    }
    return indice_minimo;
}

template <class T>
void GrafoTipo<T>::Mostrar_Seguimiento(){

    string Vertice_Nom, Costo_Min;
    cout << "Vertice\tRecorrido Corto" << endl;
    for (int i=0; i < maxVertices; i++){
        if (i < numVertices){
            Vertice_Nom = vertices[i];
        }else{
            Vertice_Nom = "--";
        }
        if (distancia[i] == INFINITO)
            Costo_Min = "Infinito";
        else
            Costo_Min = to_string(distancia[i]);
        cout << Vertice_Nom << "\t" << Costo_Min << endl;
    }
}

template <class T>
void GrafoTipo<T>::Dijkstra(T origen){
    // Si el vertice origen o que se quiere empezar no es el primer vertice ingresado
    int indice_raiz = IndiceEs(vertices, origen);
    // Se inicializa todos las distancias con "INFINITO" y el arreglo marcas con false
    for (int i = 0; i < maxVertices; i++){
        distancia[i] = INFINITO, marcas[i] = false;
    }
    distancia[indice_raiz] = 0;
    for (int k = 0; k < maxVertices - 1; k++){
        int auxiliar = Distancia_Minima();
        marcas[auxiliar] = true;
        for ( int i = 0; i < maxVertices; i++){
            if (!marcas[i] && aristas[auxiliar][i] && distancia[auxiliar] != INFINITO && distancia[auxiliar] + aristas[auxiliar][i] < distancia[i]){
                    distancia[i] = distancia[auxiliar] + aristas[auxiliar][i];
            }
        }
    }
}