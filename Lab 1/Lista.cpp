#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

// Clase Estudiante
class Estudiante
{
    private: // Atributos
        string nombre;
        int edad;
    public:
        Estudiante(string nombre, int edad){ // Constructor
            this->nombre = nombre;
            this->edad = edad;
        }
        Estudiante(){ // Constructor
            this->nombre = "";
            this->edad = 0;
        }
        string getNombre(){ // Metodo que retorna string nombre del Estudiante
            return nombre;
        }
        int getEdad(){ // Metodo que retorna int edad del Estudiante
            return edad;
        }
        void setNombre(string nombre){ // Metodo que recibe un  recibe string nombre, 
            this->nombre = nombre;
        }
        void setEdad(int edad){ // Metodo 
            this->edad = edad;
        }
};
//Por comodidad, es mejor definir la clase "Nodo" para luego definir la lista
//(cada nodo esta formado por los datos y por su "apuntador" al siguiente)
// Clase NodoEstudiante
class NodoEstudiante
{
    private: // Atributos
        Estudiante alumno;
        NodoEstudiante *siguiente;
    public:
        NodoEstudiante(){ // Constructor
            this->siguiente = NULL;
        }
        NodoEstudiante(Estudiante nuevo){ // Constructor
            this->alumno.setNombre(nuevo.getNombre());
            this->alumno.setEdad(nuevo.getEdad());
            this->siguiente = NULL;
        }
        // Metodo
        void setSiguiente(NodoEstudiante *siguiente){ 
            this->siguiente = siguiente;
        }
        // Metodo
        NodoEstudiante *getSiguiente(){ 
            return (this->siguiente);
        }
        // Metodo
        Estudiante *getEstudiante(){ 
            return (&this->alumno);
        }
        
};

// Clase ListaEstudiante
class ListaEstudiante
{
    private: // Atributos
        NodoEstudiante *primero, *ultimo;
    public:
        ListaEstudiante(){ // Constructor
            this->primero = NULL;
            this->ultimo = NULL;
        }
        // Metodo que comprueba el estado de la lista
        bool estadoActual(){
            if (this->primero == NULL && this->ultimo == NULL)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // Metodo que busca un estudiante mediante un nombre en la lista 
        bool BuscarEstudiante(string nombre_buscar){
            NodoEstudiante *recorrer;
            recorrer = primero;
            if (estadoActual()){
                return false;
            }else{
                // Recorre la lista enlazada de nodos
                while(recorrer != NULL){
                    Estudiante *estudiante;
                    estudiante = recorrer->getEstudiante();
                    // Devuelve True si lo encuentra
                    if(estudiante->getNombre() == nombre_buscar){
                        return true;
                    }else{
                        recorrer = recorrer->getSiguiente();
                    }
                }
                // Devuelve False si no lo encuentra
                return false;
            }
        }
        // Metodo Inserta un nodo(Estudiante) a la cabeza de la lista
        void insertarPrimero(Estudiante L){
            NodoEstudiante *nuevo = new NodoEstudiante(L);
            // Comprueba si no hay nada en la lista
            if (estadoActual())
            {
                // El primer nod de todo se convierte la cabeza y cola de la lista
                this->primero = nuevo;
                this->ultimo = nuevo;
            }
            else
            {   
                // Al agregar más nodos
                // El nuevo nodo pasa a ser el primero(cabeza) y el anterior pasa ser el siguiente
                nuevo->setSiguiente(this->primero);
                this->primero = nuevo;
            }
        }
        // Metodo Inserta un nodo(Estudiante) a la cola de la lista
        void insertarUltimo(Estudiante L){
            NodoEstudiante *nuevo = new NodoEstudiante(L);
            
            if (ultimo == NULL){
                // El nuevo nodo se convierte en la cabeza y cola de la lista
                primero = ultimo = nuevo;
            }else{
                // y  pasa ser el vecino de la cola y el nuevo nodo(EStudiante) pasa a ser la nueva cola de la lista 
                ultimo->setSiguiente(nuevo);
                ultimo = nuevo;
            }            
        }
        // Metodo Que muesta la lista enlazada de nodos(Estudiantes) desde el principio hasta el final
        void MostrarLista(){
            // Comprueba si no hay nada en la lista
            if (estadoActual())
            {
                Separacion();
                cout <<"      No hay nada Disponible"<< endl;
                Separacion();
            }
            else
            {
                NodoEstudiante *recorrer;
                recorrer = primero;
                Separacion();
                // Recorre la lista enlazada
                // Mostrando el nombre y edad del estudiante respectivamente
                while (recorrer != NULL)
                {
                    Estudiante *estudiante;
                    estudiante = recorrer->getEstudiante();
                    cout << "Estudiante" << endl;
                    cout << "-- Nombre: " << estudiante->getNombre() << endl;
                    cout << "-- Edad: " << estudiante->getEdad() << endl;
                    recorrer = recorrer->getSiguiente();
                }
                Separacion();
            }
        }
        // Metodo Que quita el primer nodo(cabeza) de la lista, estableciendo el siguiente como la cabeza de la lista
        void QuitarPrimero(){
            // Comprueba si no hay nada en la lista
            if (estadoActual()){
                Separacion();
                cout <<"      No hay nada Disponible"<< endl;
                Separacion();
            
            }else{
                // El primer nodo(Estudiante) se elimina de la lista
                NodoEstudiante *vecino;
                vecino = primero->getSiguiente();
                // El vecino de la cabeza de la  lista no es nulo, lo elimina
                // Si el siguiente dl primero es nulo, toma como valor nulo
                if (vecino != NULL){
                    primero = vecino;
                }else{
                    primero = ultimo = NULL;
                }
            }
        }
        // Metodo Que quita el ultimo nodo(cola) de la lista, estableciendo el anterior como la cola de la lista
        void QuitarUltimo(){
            // Comprueba si no hay nada en la lista
            if (estadoActual()){
                Separacion();
                cout <<"      No hay nada Disponible"<< endl;
                Separacion();
            }else{
                NodoEstudiante *recorrer;
                recorrer = primero;
                // Recorre la lista enlazada
                while(recorrer != NULL)
                {
                    // Si la cabeza y cola de la lista es el mismo se elimina
                    if (primero == ultimo){
                        primero = ultimo = NULL;
                        break;
                    }else{
                        // SI el siguiente es distinto nulo y el siguiente del siguiente es nulo
                        if(recorrer->getSiguiente()!= NULL && recorrer->getSiguiente()->getSiguiente() == NULL){
                            // Se convierte en la cola de la lista
                            ultimo = recorrer;
                            // El siguiente es Nulo
                            ultimo->setSiguiente(NULL);
                        }else{
                            // Pasa al siguiente
                            recorrer = recorrer->getSiguiente();
                        }
                    }
                }
            }
        }
        // Metodo separacion entre lineas 
        void Separacion(){
            cout<<"##################################"<<endl;
        }
};


int main()
{
    // tipo Variables respectivamente
    ListaEstudiante lista;
    int NumMenu;
    int edad;
    bool estado;
    string nombre;
    string nombre_buscar;
    
    string nombres[] = {"Juan", "Diego", "Sebastian", "Thomas", "Paola", "Catalina", "Sofia","Josefina","Penelope","Natalia","Cristopher"};
    srand(time(NULL));

    do{
        // Menu Interactivo con opciones 
        cout << "\n\n\t-- Menu --\n"<< endl;
        cout << "1. Mostrar Lista" << endl;
        cout << "2. Buscar Estudiante" << endl;
        cout << "3. Insertar Primer Estudiante" << endl;
        cout << "4. Insertar Ultimo Estudiante" << endl;
        cout << "5. Quitar Primero Estudiante" << endl;
        cout << "6. Quitar Ultimo Estudiante" << endl;
        cout << "7. Salir." << endl;
        cout << "#> Ingrese su opcion: ";
        cin >> NumMenu;
        cout<<"\n\n"<<endl;

        // Nombre Aleatorio
        // Edad Aleatoria 
        string nombre = nombres[rand()%11];
        int edad = 17 + rand()% 52;
        // Objeto Estudiante con su nombre y edad respectivamente
        Estudiante p(nombre,edad);

        // Ciclo segun el caso respectivo
        switch (NumMenu)
        {
            case 1:
                lista.MostrarLista(); // Muestra la lista enlazada de Nodos(Estudiantes)
                break;
            case 2:
                // Buscar nombre tipo string, recorriendo la lista enlazada de Nodos(Estudiantes)
                cout <<"Ingrese el nombre del Estudiante: ";
                cin >> nombre_buscar;
                cout<<"\n\n"<<endl;
                if(lista.BuscarEstudiante(nombre_buscar)== true){
                    lista.Separacion();
                    cout<<"      Esta Presente "<<nombre_buscar<<endl;
                    lista.Separacion();
                }else{
                    lista.Separacion();
                    cout<<"     No Esta Presente "<<nombre_buscar<<endl;
                    lista.Separacion();
                }
                break;
            case 3:
                lista.insertarPrimero(p); // Insertar Nodo(Estudiante) a la cabeza(primero) de la lista enlazada 
                break;
            case 4:
                lista.insertarUltimo(p); // Insertar Nodo(Estudiante) a la cola(ultimo) de la lista enlazada 
                break;
            case 5:
                lista.QuitarPrimero(); // Quitar Nodo(Estudiante) a la cabeza(primero) de la lista enlazada 
                break;
            case 6:
                lista.QuitarUltimo(); // Quitar Nodo(Estudiante) a la cola(ultimo) de la lista enlazada 
                break;
            case 7:
                return 0; // Termina el programa
            default:
                break; // Numero ingresado diferente a los casos anteriores
        }

    } while (NumMenu != 6); // Ciclo que se repite mientras se cumpla la condicion
}
