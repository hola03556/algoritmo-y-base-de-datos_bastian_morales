#include "graph.h"

// Metodo Construccion Grafo 
template <class T>
GrafoTipo<T>::GrafoTipo(int maxV){
    numVertices = 0;
    maxVertices = maxV;
    vertices = new T[maxV];
    aristas = new int*[maxV];
    for(int i = 0; i < maxV; i++){
        aristas[i] = new int[maxV];
    }
    distancia = new int[maxV];
    marcas = new bool[maxV];
    for (int i = 0; i < maxVertices; i++){
        marcas[i] = false;
    }
    adyacente = new list<int>[maxV];
    adyacente_costo = new list<int>[maxV];
}

// Metodo Desconstructor Grafo
template <class T>
GrafoTipo<T>::~GrafoTipo(){
    delete [] vertices;
    for(int i = 0; i < maxVertices; i++)
        delete [] aristas[i];
    delete [] aristas;
    delete [] distancia;
    delete [] marcas;
} 

// Metodo Agregar Vertices
template <class T>
void GrafoTipo<T>::Agregar_Vertice(T vertice){
    vertices[numVertices] = vertice;
    for(int indice = 0; indice < numVertices; indice++) {
        aristas[numVertices][indice] = NULL_Arista;
        aristas[indice][numVertices] = NULL_Arista;
    }
    numVertices++;
}

// Metodo Agregar Aristas
template <class T>
void GrafoTipo<T>::Agregar_Arista(T DesdeVertice, T HastaVertice, int Costo){
    int fila, col;
    fila = IndiceEs(vertices, DesdeVertice);
    col = IndiceEs(vertices, HastaVertice);
    aristas[fila][col] = Costo;
}

// Metodo Entrega el costo entre dos Vertices
template <class T>
int GrafoTipo<T>::Costo(T DesdeVertice, T HastaVertice){
    int fila, col;
    fila = IndiceEs(vertices, DesdeVertice);
    col = IndiceEs(vertices, HastaVertice);
    return aristas[fila][col];
}

// Metodo Entrega el Indice en la interseccion de Ambos Vertices
template <class T>
int GrafoTipo<T>::IndiceEs(T *vertices, T vertice){
    for(int i = 0; i < numVertices; i++){
        if (vertices[i] == vertice){
            return i;
        }
    }
    return -1;
}

// Metodo Comprueba si el Grafo esta Lleno
template <class T>
bool GrafoTipo<T>::Esta_Llena(){
    if (numVertices == maxVertices)
        return true;
    return false;
}

// Metodo Comprueba si el Grafo esta vacio
template <class T>
bool GrafoTipo<T>::Esta_Vacia(){
    if (numVertices == 0)
        return true;
    return false;
}

// Metodo Que Entrega las cantidad de Filas
template <class T>
int GrafoTipo<T>::Tamanio_Grafo(){
    return numVertices;
}
template <class T>
void GrafoTipo<T>::Agregar_Arista_B(int u, int v, int peso){
    adyacente[u].push_back(v); // Agrega V en una lista u
    adyacente_costo[u].push_back(peso); // Agrega the peso o costo de el camino bueno
}
template <class T>
void GrafoTipo<T>::Imprimir_Todos_Caminos(int s, int d)
{
    // Marca todos los vertices no visitados
    bool* visitados = new bool[maxVertices];
    // Crea una lista para los tipos de caminos
    int* Camino = new int[maxVertices];
    int* Camino_costo = new int[maxVertices];
    int Camino_indice = 0; // Inicializa  camino[] y camino_costo[] en vacio

    // Inicializa todas los vertices no visitados
    for (int i = 0; i < maxVertices; i++)
        visitados[i] = false;

    // Llama la funcion recursiva a imprimir todos los caminos
    // Inicializa el costo en 0 de la ciudad inicial
    Imprimir_Todos_Caminos_Util(s, d, visitados, Camino_costo, Camino, Camino_indice, 0);
}

// Un funcion recursiva que imprime todos los caminos desde "u" a "d"
// visitados[] guarda los vertices actuales
template <class T>
void GrafoTipo<T>::Imprimir_Todos_Caminos_Util(int u, int d, bool visitados[], int Camino_costo[],int Camino[], int& Camino_indice, int costo)
{
    // Marca el  nodo actual y  guarda in camino[]
    visitados[u] = true;
    Camino[Camino_indice] = u;
    Camino_costo[Camino_indice] = costo;   // Guarda el costo  de este paso
    Camino_indice++;
    int sum = 0;
    // si vertice actual es mismo al  destino, se imprime
    // actaul camino[]
    if (u == d) {
        for (int i = 0; i < Camino_indice; i++){
            sum += Camino_costo[i];   // Ahora agrega todos los costos
            cout << v1[Camino[i]] << " ";
        }
        cout << endl;
        cout << "Distancia Total es: " << sum;
        cout << endl;
    }else // Si vertice actual no es el destino
    {
        // Repetir para todos los vértices adyacentes al vértice actual
        // Ahora pasamos por encima de adyacente y adyacente_costo
        list<int>::iterator i, j;
        for (i = adyacente[u].begin(), j = adyacente_costo[u].begin();
            i != adyacente[u].end(); ++i, ++j)
            if (!visitados[*i])
                Imprimir_Todos_Caminos_Util(*i, d, visitados, Camino_costo, Camino, 
                                Camino_indice, *j);
    }

    // Quitar el vértice actual de camino[] y marcarlo como no visitado
    Camino_indice--;
    visitados[u] = false;
}
