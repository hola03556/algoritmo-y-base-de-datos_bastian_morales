const int NULL_Arista = 0;
#define tamanio 12
#include <iostream>
#include <list>
#include <string>
#include <vector>
using namespace std;
const vector <string> v1 = {"Moscu", "Londres", "Berlin", "Tokyo", "Madrid","Buenos Aires", "New York"};
const vector <int> w = {};
/*
Una plantilla es una manera especial de escribir funciones y clases para que estas puedan ser
usadas con cualquier tipo de dato, similar a la sobrecarga, en el caso de las funciones, pero 
evitando el trabajo de escribir cada versión de la función. En las clases, ya que no se permite hacer sobrecarga de ellas
*/
template <class T>
// Clase GrafoTipo
class GrafoTipo{
    // Metodos de la clase
    public:
        GrafoTipo(int);
        ~GrafoTipo();
        void Agregar_Vertice(T);
        void Agregar_Arista(T, T, int);
        int Costo(T, T);
        int IndiceEs(T*, T);
        bool Esta_Vacia();
        bool Esta_Llena(); 
        int Tamanio_Grafo();
        void Agregar_Arista_B(int,int,int);
        void Agregar_Vertice_A(string);
        void Imprimir_Todos_Caminos(int,int);
        void Imprimir_Todos_Caminos_Util(int, int, bool*, int*, int*, int&, int);
    // Atributos de la clase
    private:
        int numVertices;
        int maxVertices;
        T* vertices;
        int **aristas;
        bool* marcas;
        int *distancia;
        list<int>* adyacente;
        list<int>* adyacente_costo;
        
}; 
