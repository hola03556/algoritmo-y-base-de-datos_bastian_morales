#include "Graph.cpp"

int main() {
    /*
    El algoritmo DFS comienza en el nodo raíz (seleccionando algún nodo 
    arbitrario como nodo raíz en el caso de un gráfico) y explora lo más lejos 
    posible a lo largo de cada rama antes de retroceder.
    pasar al nodo adyacente sin marcar y continuar este bucle hasta que no haya un nodo adyacente sin marcar. 
    Luego retroceda y verifique si hay otros nodos sin marcar y atraviese. Finalmente, imprima los nodos en la ruta.

    El inconveniente del algoritmo DFS no toma en cuenta el peso o costo entre cada nodos, solo busca visitar todos los nodos
    entre el nodo base (partida) y el nodo final (meta)
    */
    GrafoTipo<int> g(7);
    // Agregar Vertice entre (Origen,Destino,Costo)
    g.Agregar_Arista_B(0, 1, 1750);
    g.Agregar_Arista_B(0, 5, 2380);
    g.Agregar_Arista_B(1, 3, 4039);
    g.Agregar_Arista_B(2, 5, 3750); 
    g.Agregar_Arista_B(2, 6, 11550);
    g.Agregar_Arista_B(2, 3, 1303);
    g.Agregar_Arista_B(3, 4, 5782);
    g.Agregar_Arista_B(3, 6, 12947);
    g.Agregar_Arista_B(4, 2, 200);
    g.Agregar_Arista_B(5, 3, 8231);
    // Inicio   Final
    int s = 0, d = 2;
    // Caminos posibles desde el origen hasta el final
    cout << "Todos los caminos diferentes desde " << v1[s] << " a " << v1[d] << endl;
    // Metodo para imprimir los caminos
    g.Imprimir_Todos_Caminos(s, d);
    return 0;
}