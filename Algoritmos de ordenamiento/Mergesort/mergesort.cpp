#include<iostream>
#include<vector>
#include<cstdlib>
#include<ctime>

#include "mergesort.h"
using namespace std;

MergeSort::MergeSort(int tamannoArreglo){
    tamanno = (tamannoArreglo > 0) ? tamannoArreglo: 10;
    srand(time(0));

    for(int i=0; i< tamanno; i++)
        datos.push_back(10 + rand() % 90);
}

void MergeSort::ordenar(){
    ordenarSubArreglo(0, tamanno-1);
}

void MergeSort::ordenarSubArreglo(int inferior, int superior){
    //Se puede "particionar" el arreglo???
    if (inferior < superior){
        int mitad = (inferior + superior) / 2;

        cout << "Dividiendo!\n";
        mostrarSubArreglo(inferior, mitad);
        cout << "\n        ";
        mostrarSubArreglo(mitad+1, superior);
        cout << "\n\n";

        ordenarSubArreglo(inferior, mitad);
        ordenarSubArreglo(mitad+1, superior);

        mezclar(inferior, mitad, mitad+1, superior);
    }
}

void MergeSort::mezclar(int inicio1, int fin1, int inicio2, int fin2){
    int indice1 = inicio1;
    int indice2 = inicio2;

    int indiceNuevo = inicio1;
    vector<int> combinacionArreglo(tamanno);

    //////Se muestran los dos subarreglos a mezclar
    cout << "A mezclar:  ";
    mostrarSubArreglo(inicio1, fin1);
    cout << "\n";
    mostrarSubArreglo(inicio2, fin2);
    cout << "\n";


    while (indice1 <= fin1 && indice2 <= fin2){
        if (datos[indice1] <= datos[indice2])
            combinacionArreglo[indiceNuevo++] = datos[indice1++];
        else
            combinacionArreglo[indiceNuevo++] = datos[indice2++];
    }

    if (indice1 == inicio2){
        while (indice2 <= fin2)
            combinacionArreglo[indiceNuevo++] = datos[indice2++];
    }else{
        while (indice1 <= fin1)
            combinacionArreglo[indiceNuevo++] = datos[indice1++];
    } 

    //Copiar Arreglo de Combinacion en Arreglo de Datos original
    for(int i=inicio1; i <= fin2; i++)
        datos[i] = combinacionArreglo[i];
    
    cout << "      ";
    mostrarSubArreglo(inicio1, fin2);
    cout << "\n\n";
}

void MergeSort::mostrarElementos(){
    mostrarSubArreglo(0, tamanno-1);
}

void MergeSort::mostrarSubArreglo(int inferior, int superior){
    for(int i=0; i< inferior; i++)
        cout << " ";
    
    for(int i=inferior; i<=superior; i++)
        cout << " " << datos[i];
}