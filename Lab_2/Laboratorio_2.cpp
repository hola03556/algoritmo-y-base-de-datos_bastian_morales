#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <ctype.h>

using namespace std;

// Valores 

class Valor
{
    private: // Atributos
        float numero;
    public:
        Valor(){ // Constructor
            this->numero = 0;
        }
        Valor(float numero){ // Constructor
            this->numero = numero;
        }
        float getNumero(){
            return numero;
        }
        void setNumero(float numero){
            this->numero = numero;
        }
};
class Nodo_Valor
{
    private: // Atributos
        Valor number;
        Nodo_Valor *siguiente;
    public:
        Nodo_Valor(){ // Constructor
            this->siguiente = NULL;
        }
        Nodo_Valor(Valor nuevo){ // Constructor
            this->number.setNumero(nuevo.getNumero());
            this->siguiente = NULL;
        }
        void setSiguiente(Nodo_Valor *siguiente){
            this->siguiente = siguiente;
        }
        Nodo_Valor *getSiguiente(){
            return (this->siguiente);
        }
        Valor *getValor(){
            return (&this->number);
        }
        
};
class Stack_Valor{
    private:
        Nodo_Valor *primero;
    public:
        Stack_Valor(){
            this->primero = NULL;
        }

        bool IsEmpty(){
            if (this->primero == NULL)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        float numero_sacado(){
		    return primero->getValor()->getNumero();
        }

        void Push(Valor L){
            Nodo_Valor *nuevo = new Nodo_Valor(L);
            if(IsEmpty()){
                this->primero = nuevo;
                return;
            }else{
                nuevo->setSiguiente(this->primero);
                this->primero = nuevo;
            }
        }

        void Pop(){
            if (IsEmpty()== true){
                return;
            }else{
                Nodo_Valor *aux;
                aux = primero;
                primero = primero->getSiguiente();
                delete aux;
            }
        }

        int count() {
            Nodo_Valor *recorrer;
            recorrer = primero;
            int total = 0;

            while (recorrer != NULL) {
                total++;
                recorrer = recorrer->getSiguiente();
            }

            return total;
        }

        void clean(){
            while(!IsEmpty()){
                Nodo_Valor *aux;
                aux = primero;
                primero = primero->getSiguiente();
                delete aux;
            }
        }

        void Display(){
            if (IsEmpty()){
                cout <<""<< endl;
            }else{
                Nodo_Valor *recorrer;
                recorrer = primero;
	            while(recorrer != NULL){
                    Valor *digito;
                    digito = recorrer->getValor();
		            cout << "\t|\t" << digito->getNumero() << "\t|" << endl;
                    recorrer = recorrer->getSiguiente();
                }
	            cout << "\t|---------------|" << endl;
            }
        }
};



// Operadores 

class Operacion
{
    private: // Atributos
        string operador;
        int precedencia;
    public:
        Operacion(string operador){ // Constructor
            this->operador = operador;
            if (operador == "+" || operador == "-"){
                setPrecedencia(1);
            }else if (operador == "*" || operador == "/"){
                setPrecedencia(2);
            }else{
                setPrecedencia(3);
            }
            
        }
        Operacion(){ // Constructor

        }
        string getOperador(){
            return operador;
        }
        int getPrecedencia(){
            return precedencia;
        }
        void setOperador(string operador){
            this->operador = operador;
        }
        void setPrecedencia(int precedencia){
            this->precedencia = precedencia;
        }
};
class Nodo_Operacion
{
    private: // Atributos
        Operacion *arit;
        Nodo_Operacion *siguiente;
    public:
        Nodo_Operacion(){ // Constructor
            this->siguiente = NULL;
        }
        Nodo_Operacion(Operacion *nuevo){ // Constructor
            arit = nuevo;
            this->siguiente = NULL;
        }
        void setSiguiente(Nodo_Operacion *siguiente){
            this->siguiente = siguiente;
        }
        Nodo_Operacion *getSiguiente(){
            return (this->siguiente);
        }
        Operacion *getOperacion(){
            return (this->arit);
        }
        
};
class Stack_Operador{
    private:
        Nodo_Operacion *primero;
    public:
        Stack_Operador(){
            this->primero = NULL;
        }

        bool IsEmpty(){
            if (this->primero == NULL)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        string Primero(){
            return primero->getOperacion()->getOperador();
        }

        int Jeraquia(){
		    return primero->getOperacion()->getPrecedencia();
        }
        int cantidad(){
            Nodo_Operacion *recorrer;
            int total = 0;
            recorrer = primero;
            while(recorrer != NULL){
                Operacion *mate;
                recorrer = recorrer->getSiguiente();
                total ++;
            }
            return total;
        }
        void Push(Operacion *L){
            Nodo_Operacion *nuevo = new Nodo_Operacion(L);
            if(IsEmpty()){
                this->primero = nuevo;
                return;
            }else{
                nuevo->setSiguiente(this->primero);
                this->primero = nuevo;
                return;
            }
        }

        void Pop(){
            if (IsEmpty()== true){
                return;
            }else{
                Nodo_Operacion *aux;
                aux = primero;
                primero = primero->getSiguiente();
                delete aux;
            }
        }
        void clean(){
            while(!IsEmpty()){
                Nodo_Operacion *aux;
                aux = primero;
                primero = primero->getSiguiente();
                delete aux;
            }
        }

        void Display(){
            if (IsEmpty()){
                cout <<""<< endl;
            }else{
                Nodo_Operacion *recorrer;
                recorrer = primero;
	            while(recorrer != NULL){
                    Operacion *mate;
                    mate = recorrer->getOperacion();
		            cout << "\t|\t" << mate->getOperador()  << "\t|" << endl;
                    recorrer = recorrer->getSiguiente();
                }
	            cout << "\t|---------------|" << endl;
            }
        }
};


float calculadora (float num_1, float num_2,string Primer,int Jerar){
            float resultado;
            if (Primer == "+" ){
                resultado = num_2 + num_1;
            }
            if (Primer == "-"){
                resultado = num_2 - num_1;
            }
            if (Primer == "*"){
                resultado = num_2 * num_1;
            }
            if (Primer == "/"){
                resultado = num_2 / num_1;
            }
            return resultado;
        }




int main(){
    Stack_Valor stack_value;
    Stack_Operador stack_operator;
    string expresion_mate;
    string signo_aux;
    int parentesis = 0,jerarquia_aux;
    float valor_float, resultado,num_2,num_1;


    cout << "Ingrese expresion matematica: ";
    getline(cin, expresion_mate);
    // cout << expresion_mate << endl;
    string numeroActual = "";
    string arreglo[expresion_mate.length()];
    int componentes = 0;
    int i = 0;

    // ####################################################
    for(int i=0; i < expresion_mate.length(); i++){
        string actual = expresion_mate.substr(i,1);
        if (actual == "+" || actual == "-" || actual == "*" || actual == "/" || actual == " " || actual == "(" || actual == ")"){
            if(actual == " "){
                continue;
            }else{
                if (actual == "("){
                    parentesis++;
                }
                arreglo[componentes] = numeroActual;
                float valor_float = stof(numeroActual);
                Valor Numero (valor_float);
                stack_value.Push(Numero);
                componentes++;
                arreglo[componentes] = actual;
                Operacion *signo = new Operacion(actual);
                stack_operator.Push(signo);
                componentes++;
                numeroActual = "";
            }
        }else{
            numeroActual += actual;
        }    
    }
    if (numeroActual != ""){
        arreglo[componentes] = numeroActual;
        float valor_float = stof(numeroActual);
        Valor Numero (valor_float);
        stack_value.Push(Numero);
        componentes++;
    }
    //##################################################
    cout<<"\n\n\n"<<endl;
    stack_value.Display();
    cout << "\t|\t" << "Valores"  << "\t|" << endl;
    stack_value.clean();
    cout<<"\n\n"<<endl;
    stack_operator.Display();
    cout << "\t|\t" << "Operadores"  << "\t|" << endl;
    stack_operator.clean();

    for(int i=0; i < componentes; i++){
        string presente = arreglo[i];
        if (parentesis == 0){
            if (presente != "+" && presente != "-" && presente != "*" && presente != "/"  &&  presente != "(" && presente != ")"){
                float valor_float = stof(presente);
                Valor Numero (valor_float);
                stack_value.Push(Numero);
            }else if (presente == "+" || presente == "-" || presente == "*" || presente == "/"){
                Operacion *signo = new Operacion(presente);
                if (stack_operator.IsEmpty() == true){
                    stack_operator.Push(signo);
                    signo_aux = presente;
                    jerarquia_aux = signo->getPrecedencia();
                    continue;
                }else{
                    while (true) {
                        if (jerarquia_aux >= signo->getPrecedencia()){
                                num_1 = stack_value.numero_sacado();
                                stack_value.Pop();
                                num_2 = stack_value.numero_sacado();
                                stack_value.Pop();
                                if (signo_aux == "+" ){
                                    resultado = num_2 + num_1;
                                }else if (signo_aux == "-"){
                                    resultado = num_2 - num_1;
                                }else if (signo_aux == "*"){
                                    resultado = num_2 * num_1;
                                }else if (signo_aux == "/"){
                                    resultado = num_2 / num_1;
                                }
                                stack_operator.Pop();
                                stack_value.Push(Valor(resultado));
                                if (stack_operator.IsEmpty()){
                                    break;
                                } else {
                                    signo_aux = stack_operator.Primero();
                                    jerarquia_aux = stack_operator.Jeraquia();
                                }
                                
                        } else {
                            break;
                        }
                    }
                    stack_operator.Push(signo);
                    signo_aux = stack_operator.Primero();
                    jerarquia_aux = stack_operator.Jeraquia();
                }
            }
        }else{
            //cout << "\tactual: " << presente << endl;
        }
        
        
    }
    if (parentesis == 0){
        while (stack_value.count() > 1){
            float num_1 = stack_value.numero_sacado();
            stack_value.Pop();
            float num_2 = stack_value.numero_sacado();
            stack_value.Pop();
            signo_aux = stack_operator.Primero();
            jerarquia_aux = stack_operator.Jeraquia();
            if (signo_aux == "+" ){
                resultado = num_2 + num_1;
            }else if (signo_aux == "-"){
                resultado = num_2 - num_1;
            }else if (signo_aux == "*"){
                resultado = num_2 * num_1;
            }else if (signo_aux == "/"){
                resultado = num_2 / num_1;
            }
            stack_operator.Pop();
            stack_value.Push(Valor(resultado));
        }
    }

    cout<<"El resultado de la expresion "<< expresion_mate <<" es: "<<stack_value.numero_sacado() <<endl;







    return 0;
};