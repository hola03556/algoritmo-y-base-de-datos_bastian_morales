#include "graph.h"

GraphType::GraphType(int maxV){
   numVertices = 0;
   maxVertices = maxV;
   vertices = new int[maxV];
   edges = new int*[maxV];
 
   for(int i = 0; i < maxV; i++)
     edges[i] = new int[maxV];
 
   marks = new bool[maxV];
}

GraphType::~GraphType(){
   delete [] vertices;
 
   for(int i = 0; i < maxVertices; i++)
      delete [] edges[i];
 
   delete [] edges;
   delete [] marks;
} 

void GraphType::AddVertex(int vertex){
   vertices[numVertices] = vertex;

   for(int index = 0; index < numVertices; index++) {
     edges[numVertices][index] = NULL_EDGE;
     edges[index][numVertices] = NULL_EDGE;
   }
   
   numVertices++;
}

void GraphType::AddEdge(int fromVertex, int toVertex, int weight){
   int row, col;

   row = IndexIs(vertices, fromVertex);
   col = IndexIs(vertices, toVertex);
   edges[row][col] = weight;
} 

int GraphType::WeightIs(int fromVertex, int toVertex){
   int row, col;

   row = IndexIs(vertices, fromVertex);
   col = IndexIs(vertices, toVertex);
   
   return edges[row][col];
} 

int GraphType::IndexIs(int *vertices, int vertex){
  for(int i = 0; i < numVertices; i++){
      if (vertices[i] == vertex)
          return i;
  }

  return -1;
}

bool GraphType::IsFull(){
  if (numVertices == maxVertices)
      return true;
  
  return false;
}

bool GraphType::IsEmpty(){
  if (numVertices == 0)
      return true;
  
  return false;
}

int GraphType::GraphSize(){
  return numVertices;
}