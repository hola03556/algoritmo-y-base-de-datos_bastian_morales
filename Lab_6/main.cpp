#include <iostream>
#include <string>
#include <stdlib.h>
#include <list>
#include <vector>
#include <chrono>
using namespace std;
using namespace std::chrono; // https://www.geeksforgeeks.org/chrono-in-c/

// ********** Burbuja ***********
void swap(int *uno, int *dos){
    int aux = *uno;
    *uno = *dos;
    *dos = aux;
}
void bubblesort(int arr[], int n)
{
	for (int i = 0; i < n - 1; i++){
        for(int j=0; j < n - i - 1; j++){
            if (arr[j] > arr[j + 1]){
                swap(&arr[j], &arr[j+1]);
            }
        }
    }	
}
// ********* Arreglo Con o Sin Orden ***********

int eleccion(){
    int opcion;
    do{
        cout << "\n\t************" << endl;
        cout << "\t* Eleccion *" << endl;
        cout << "\t************" << endl;
        cout << "[1]. Ingreso de Datos de Forma Manual " << endl;
        cout << "[2]. Ingreso de Datos de Forma Random " << endl;
        cout << "-> Eleccion: ";
        cin >> opcion;
        if (opcion != 1 && opcion != 2){
            cout << "\t***********************" << endl;
            cout << "\t* Eleccion incorrecta *" << endl;
            cout << "\t***********************\n" << endl;
        }
    }while(opcion != 1 && opcion != 2);
    return opcion;
}
//********** Merge Sort *************
void merge(int arreglo[], int const left, int const centro, int const right)
{
    auto const subArregloUno = centro - left + 1;
    auto const subArregloDos = right - centro;
    auto *izqArreglo = new int[subArregloUno], *derArreglo = new int[subArregloDos];

    for (auto i = 0; i < subArregloUno; i++)
        izqArreglo[i] = arreglo[left + i];
    for (auto j = 0; j < subArregloDos; j++)
        derArreglo[j] = arreglo[centro + 1 + j];

    auto indexDeSubArregloUno = 0, indexDeSubArregloDos = 0;
    int indexDeMergedArreglo = left;

    while (indexDeSubArregloUno < subArregloUno && indexDeSubArregloDos < subArregloDos){
        if (izqArreglo[indexDeSubArregloUno] <= derArreglo[indexDeSubArregloDos]) {
            arreglo[indexDeMergedArreglo] = izqArreglo[indexDeSubArregloUno];
            indexDeSubArregloUno++;
        }
        else {
            arreglo[indexDeMergedArreglo] = derArreglo[indexDeSubArregloDos];
            indexDeSubArregloDos++;
        }
        indexDeMergedArreglo++;
    }

    while (indexDeSubArregloUno < subArregloUno) {
        arreglo[indexDeMergedArreglo] = izqArreglo[indexDeSubArregloUno];
        indexDeSubArregloUno++;
        indexDeMergedArreglo++;
    }
    while (indexDeSubArregloDos < subArregloDos) {
        arreglo[indexDeMergedArreglo] = derArreglo[indexDeSubArregloDos];
        indexDeSubArregloDos++;
        indexDeMergedArreglo++;
    }
    delete[] izqArreglo;
    delete[] derArreglo;
}
void mergeSort(int arreglo[], int const inicio, int const end)
{
    if (inicio >= end)
        return;
    auto centro = inicio + (end - inicio) / 2;
    mergeSort(arreglo, inicio, centro);
    mergeSort(arreglo, centro + 1, end);
    merge(arreglo, inicio, centro, end);
}
//*************** Formateo de Arreglo ****************
void formateo_arreglo(int arreglo[], int arreglo_respaldo[], int Tamanio){
    for (int i = 0; i < Tamanio; i++){
        arreglo[i] = arreglo_respaldo[i];
    }
}
// ************* Imprimir Arreglo *********************
void Imprimir_Arreglo(int arreglo[], int Tamanio){
    string Impresion = "";
    cout << "\n********************************************************" << endl;
    for (int i = 0; i < Tamanio; i++){
        if (i < Tamanio - 1){
            Impresion += "[" + to_string(arreglo[i]) + "] - ";
        }else{
            Impresion += "[" + to_string(arreglo[i]) + "]";
        }
        
    }
    cout << "Arreglo: " << Impresion << endl;
    cout << "********************************************************" << endl;
}
//*************************************


int main(){
    int NumMenu, Tamanio, opcion, Valor;

    cout << "\n\n################################################" << endl;
    cout << "Ingrese el tamanio del Arreglo (mayor a 0): ";
    cin >> Tamanio;
    cout << "################################################" << endl;
    opcion = eleccion();
    int arreglo[Tamanio], arreglo_respaldo[Tamanio];

    if (opcion == 1){
        for (int i = 0; i < Tamanio; i++){
            cout << "Ingrese el elemento " << "[" << i+1 << "]: ";
            cin >> Valor;
            arreglo[i] = Valor;
            arreglo_respaldo[i] = Valor;
        }    
    }else if (opcion ==2){
        srand(time(0));
        for (int j = 0; j < Tamanio; j++){
            Valor = 1 + rand() % 100;
            arreglo[j] = Valor;
            arreglo_respaldo[j] = Valor;
        }
    }

    auto Arreglo_Tamanio = sizeof(arreglo) / sizeof(arreglo[0]);

    do{ 
        // Opciones correspondientes
        cout << "\n\n\t-- Menu Busqueda --\n"<< endl;
        cout << "[1]. Mostrar datos de arreglo original sin ordenar" << endl;
        cout << "[2]. Mostrar datos del arreglo ordenado mediante Algoritmo Burbuja " << endl;
        cout << "[3]. Mostrar datos del arreglo ordenado mediante Algoritmo Mergesort " << endl;
        cout << "[4]. Mostrar tiempo de ejecucion en milisegundos de los Algoritmos" << endl;
        cout << "[5]. Salir." << endl;
        cout << "#> Ingrese su opcion: ";
        cin >> NumMenu;

        switch(NumMenu){
            case 1:
                Imprimir_Arreglo(arreglo,Tamanio);
                break;
            case 2:
                {
                    bubblesort(arreglo, Tamanio);
                    Imprimir_Arreglo(arreglo,Tamanio);
                    formateo_arreglo(arreglo, arreglo_respaldo, Tamanio);
                }
                break;
            case 3:
                {
                    mergeSort(arreglo, 0, Arreglo_Tamanio - 1);
                    Imprimir_Arreglo(arreglo,Tamanio);
                    formateo_arreglo(arreglo, arreglo_respaldo, Tamanio);
                }
                break;
            case 4:
                {
                    auto Inicio_1 = std::chrono::high_resolution_clock::now();
                    bubblesort(arreglo, Tamanio);
                    auto Fin_1 = high_resolution_clock::now();
                    auto Duracion_1 = duration_cast<microseconds>(Inicio_1-Fin_1);
                    formateo_arreglo(arreglo, arreglo_respaldo, Tamanio);
                    auto Inicio_2 = high_resolution_clock::now();
                    mergeSort(arreglo, 0, Arreglo_Tamanio - 1);
                    auto Fin_2 = high_resolution_clock::now();
                    auto Duracion_2 = duration_cast<microseconds>(Inicio_2-Fin_2);
                    cout << "\n\t*************************" << endl;
                    cout << "\t* Tiempo de Ejecuciones *" << endl;
                    cout << "\t*************************" << endl;
                    cout << "Tiempo de Ejecucion del Algoritmo Burbuja: " << Duracion_1.count() << " microseconds" << endl;
                    cout << "Tiempo de Ejecucion del Algoritmo Mergesort: " << Duracion_2.count() << " microseconds" << endl;
                }
                break;
            case 5:
                return 0;
            default:
                break;
        }
    }while (NumMenu != 5);
    return 0;
}